package silva.danilo.appprojetotcc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import silva.danilo.appprojetotcc.adapter.ListaVeiculosAdapter;
import silva.danilo.appprojetotcc.model.VeiculoJson;
import silva.danilo.appprojetotcc.webclient.TesteWebClient;
import silva.danilo.appprojetotcc.webclient.veiculos.CarregadorListaVeiculos;

public class ListaVeiculosActivity extends AppCompatActivity {

    ListView listaVeiculos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_veiculos);

        listaVeiculos = (ListView) findViewById(R.id.lista_veiculos);

        listaVeiculos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                VeiculoJson veiculo = (VeiculoJson) listaVeiculos.getItemAtPosition(position);

                Intent i = new Intent(ListaVeiculosActivity.this, LocalVeiculoActivity.class);
                i.putExtra("veiculoSelecionado", veiculo);

                startActivity(i);

            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        new CarregadorListaVeiculos(this).execute();
    }



}
