package silva.danilo.appprojetotcc.webclient.veiculos;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import silva.danilo.appprojetotcc.ListaVeiculosActivity;
import silva.danilo.appprojetotcc.configuracoes.ParametrosConfig;
import silva.danilo.appprojetotcc.exception.ExceptionRequisicao;
import silva.danilo.appprojetotcc.model.dto.RespostaAutenticacaoDto;
import silva.danilo.appprojetotcc.model.form.UsuarioForm;
import silva.danilo.appprojetotcc.webclient.WebClient;

public class ServicoAutenticacaoWebClient extends AsyncTask<Void, Void, String>
{
    private UsuarioForm usuarioForm;
    private Activity activity;

    public ServicoAutenticacaoWebClient(Activity activity, UsuarioForm usuarioForm)
    {
        this.usuarioForm = usuarioForm;
        this.activity = activity;
    }


    @Override
    protected String doInBackground(Void... voids) {

        Log.d("POST", "chamou do in background");
        WebClient client = new WebClient();
        String response = null;

        try
        {
            response = client.request(WebClient.URL_SERVICO_AUTENTICACAO, usuarioForm.toJson(), "POST");

            return response;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        Log.d("POST", "response do in background: " + response);
        return response;
    }

    @Override
    protected void onPostExecute(String resposta) {

        Log.d("RES POST: ", "" + resposta);

        if(resposta != null)
        {
            RespostaAutenticacaoDto resp = new Gson().fromJson(resposta, RespostaAutenticacaoDto.class);

            ParametrosConfig.tokenAutenticacao = resp.getToken().retornaModelo();

            Intent i = new Intent(activity, ListaVeiculosActivity.class);

            activity.startActivity(i);
        }

    }
}
