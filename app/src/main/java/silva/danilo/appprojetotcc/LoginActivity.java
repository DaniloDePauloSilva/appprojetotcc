package silva.danilo.appprojetotcc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import silva.danilo.appprojetotcc.model.form.UsuarioForm;
import silva.danilo.appprojetotcc.webclient.AutenticacaoWebClient;
import silva.danilo.appprojetotcc.webclient.veiculos.ServicoAutenticacaoWebClient;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText txtEmail;
    private EditText txtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button) findViewById(R.id.login_btnLogin);

        txtEmail = findViewById(R.id.login_txtLogin);
        txtSenha = findViewById(R.id.login_txtSenha);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TESTE", "click");
                new ServicoAutenticacaoWebClient(LoginActivity.this, new UsuarioForm(txtEmail.getText().toString(), txtSenha.getText().toString())).execute();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();



    }
}
