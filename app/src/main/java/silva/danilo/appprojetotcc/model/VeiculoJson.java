package silva.danilo.appprojetotcc.model;

import java.io.Serializable;

public class VeiculoJson implements Serializable {

    private Integer id;
    private String descricao;
    private String placa;
    private String dataCadastro;

    //private LocalizacaoJson ultimaLocalizacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
